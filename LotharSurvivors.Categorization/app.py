import pandas as pd
from flask import Flask, request, jsonify
from sklearn.externals import joblib

app = Flask(__name__)

@app.route("/")
def hello():
    return "Hello from Categorization Service!"

@app.route("/categorization", methods=["POST"])
def predict():
     if request.method == "POST":
        try:
            data = request.get_json()
            text = data["text"]

            categorization_model = joblib.load("./model.pkl")
            category = categorization_model.predict([text])

            return jsonify(results = category.tolist())
        except ValueError:
            return jsonify("Please enter a correct data.")

if __name__ == "__main__":
    app.run(host="0.0.0.0", debug=True, port=6970)