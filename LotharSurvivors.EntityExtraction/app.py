import nltk
from flask import Flask, request, jsonify

app = Flask(__name__)

def remove_punctuation(text):
    punctuation = ['.', ',', ';', ':', '(', ')', '[', ']', '{', '}', '\"', '\'','\'\'', '\`', '\`\`', '==', '===', '====', '-']
    text_stripped = [w for w in text if w not in punctuation]
    text_stripped = [w for w in text if len(w) > 2]
    return text_stripped

def strip_text(text):
    text_stripped = remove_punctuation(text)
    return text_stripped

@app.route("/")
def hello():
    return "Hello from Entity Extraction!"

@app.route("/extract", methods=["POST"])
def predict():
     if request.method == "POST":
        try:
            data = request.get_json()
            text = data["text"]
            gvb_korpus = nltk.corpus.PlaintextCorpusReader("Corpus/", ".*")
            gvb_word_list = [w for w in gvb_korpus.words()]

            matches = []
            for w in gvb_word_list:
                if w in text and w not in matches:
                    matches.append(w)

            return jsonify(results = matches)
        except ValueError:
            return jsonify("Please enter a correct data.")

if __name__ == "__main__":
    app.run(host="0.0.0.0", debug=True, port=6969)