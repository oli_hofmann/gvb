Write-Host "Logging in.."
az acr login --name cke0218registry

Write-Host "Tagging everything..."
docker tag lotharsurvivorsapi cke0218registry.azurecr.io/lotharsurvivorsapi:auto
docker tag lotharsurvivorscategorization cke0218registry.azurecr.io/lotharsurvivorscategorization:auto
docker tag lotharsurvivorsentityextraction cke0218registry.azurecr.io/lotharsurvivorsentityextraction:auto
docker tag lotharsurvivorsfrontend cke0218registry.azurecr.io/lotharsurvivorsfrontend:auto

Write-Host "Done, now pushing to docker registry..."
docker push cke0218registry.azurecr.io/lotharsurvivorsapi:auto
docker push cke0218registry.azurecr.io/lotharsurvivorscategorization:auto
docker push cke0218registry.azurecr.io/lotharsurvivorsentityextraction:auto
docker push cke0218registry.azurecr.io/lotharsurvivorsfrontend:auto