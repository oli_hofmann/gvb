﻿namespace LotharSurvivors.Models.Constants
{
    public enum CraftmanDamagedObjectOfferStatus
    {
        Undefined = 0,

        ClientRequestsOffer = 100,

        CraftmanIsNotInterested = 901,

        CraftmanIsInterested = 200,

        RepairCompleted = 800,

        OfferDeclinedByCustomer = 902
    }
}