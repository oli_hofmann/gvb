﻿namespace LotharSurvivors.Models.Constants
{
    public enum CaseStatus
    {
        Undefined,
        Preparation,
        InProgress,
        Closed
    }
}