﻿using System.Collections.Generic;

namespace LotharSurvivors.Models.Database
{
    public class Client : BaseEntity
    {
        public string FirstName { get; set; }

        public string LastName { get; set; }

        public string Street { get; set; }

        public string ZipCode { get; set; }

        public string City { get; set; }

        public IEnumerable<DamageCase> DamageReportCases { get; set; } = new List<DamageCase>();
    }
}
