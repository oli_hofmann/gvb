﻿using LotharSurvivors.Models.Constants;

namespace LotharSurvivors.Models.Database
{
    public class CraftmanDamagedObjectOffer : BaseEntity
    {
        public long DamagedObjectCaseId { get; set; }

        public DamagedObjectCase DamagedObjectCase { get; set; }

        public long CraftmanId { get; set; }

        public Craftman Craftman { get; set; }

        public CraftmanDamagedObjectOfferStatus Status { get; set; }

        public double Rating { get; set; }

        public string Review { get; set; }
    }
}
