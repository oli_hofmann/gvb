﻿using System;
using System.Collections.Generic;

namespace LotharSurvivors.Models.Database
{
    public class DamageCase : BaseEntity
    {
        public DateTime DateOfEvent { get; set; }

        public string EventDescription { get; set; }

        public Client Client { get; set; }

        public IEnumerable<DamagedObjectCase> DamagedObjectCases { get; set; } = new List<DamagedObjectCase>();

        public string Token { get; set; }
    }
}
