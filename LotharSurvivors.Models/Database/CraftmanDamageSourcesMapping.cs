﻿using System;
using System.Collections.Generic;
using System.Text;

namespace LotharSurvivors.Models.Database
{
    public class CraftmanDamageSourcesMapping : BaseEntity
    {
        public Craftman Craftman { get; set; }

        public DamageSource DamageSource { get; set; }
    }
}
