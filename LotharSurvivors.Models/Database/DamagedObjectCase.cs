﻿using System.Collections.Generic;

namespace LotharSurvivors.Models.Database
{
    public class DamagedObjectCase : BaseEntity
    {
        public Client Client { get; set; }

        public string Name { get; set; }

        public IEnumerable<DamagedObjectCaseDamageSourceMapping> DamagedObjectCaseDamageSourceMappings { get; set; } 
            = new List<DamagedObjectCaseDamageSourceMapping>();

        public DamageCase DamageCase { get; set; }

        public IEnumerable<CraftmanDamagedObjectOffer> Offers { get; set; }
    }
}
