﻿using System.Collections.Generic;

namespace LotharSurvivors.Models.Database
{
    public class Craftman : BaseEntity
    {
        public string FirstName { get; set; }

        public string LastName { get; set; }

        public string CompanyName { get; set; }

        public string Street { get; set; }

        public string ZipCode { get; set; }

        public string City { get; set; }

        public string EMail { get; set; }

        public string PhoneNumber { get; set; }

        public string Description { get; set; }

        public string ImageUri { get; set; }

        public bool Activated { get; set; } = true;

        public IEnumerable<CraftmanDamageSourcesMapping> CraftmanDamageSourcesMappings { get; set; } = new List<CraftmanDamageSourcesMapping>();

        public IEnumerable<CraftmanDamagedObjectOffer> Offers { get; set; } = new List<CraftmanDamagedObjectOffer>();
    }
}
