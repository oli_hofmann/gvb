﻿using System.Collections.Generic;

namespace LotharSurvivors.Models.Database
{
    public class DamageSource : BaseEntity
    {
        public string Name { get; set; }

        public IEnumerable<CraftmanDamageSourcesMapping> CraftmanDamageSourcesMappings { get; set; }

        public IEnumerable<DamagedObjectCaseDamageSourceMapping> DamagedObjectCaseDamageSourceMappings { get; set; }
    }
}
