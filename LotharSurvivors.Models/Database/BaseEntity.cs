﻿using System.ComponentModel.DataAnnotations;

namespace LotharSurvivors.Models.Database
{
    public abstract class BaseEntity
    {
        [Key]
        public long Id { get; set; }
    }
}
