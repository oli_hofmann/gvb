﻿namespace LotharSurvivors.Models.Database
{
    public class DamagedObjectCaseDamageSourceMapping : BaseEntity
    {
        public long DamagedObjectCaseId { get; set; }

        public DamagedObjectCase DamagedObjectCase { get; set; }

        public long DamageSourceId { get; set; }

        public DamageSource DamageSource { get; set; }
    }
}
