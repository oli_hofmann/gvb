﻿namespace LotharSurvivors.Models.DTOs
{
    public class OfferCompletionDto
    {
        public double Rating { get; set; }

        public string Review { get; set; }
    }
}
