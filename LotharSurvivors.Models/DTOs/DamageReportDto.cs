﻿using System;
using System.Collections.Generic;

namespace LotharSurvivors.Models.DTOs
{
    public class DamageReportDto : AddressDto
    {
        public DateTime DateOfEvent { get; set; }

        public string EventDescription { get; set; }

        public IEnumerable<string> DamagedObjects { get; set; }
    }
}
