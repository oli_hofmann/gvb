﻿using System.Collections.Generic;

namespace LotharSurvivors.Models.DTOs
{
    public class DamagedObjectCaseDto
    {
        public long Id { get; set; }

        public string Name { get; set; }

        public IEnumerable<string> DamageSources { get; set; }

        public IEnumerable<OfferDto> Offers { get; set; }

        public IEnumerable<CraftmanDto> Craftsmen { get; set; }
    }
}