﻿using System.Collections.Generic;

namespace LotharSurvivors.Models.DTOs
{
    public class CraftmanDto : AddressDto
    {
        public long Id { get; set; }

        public string CompanyName { get; set; }

        public string Description { get; set; }

        public string ImageUrl { get; set; }

        public double StarRating { get; set; }

        public IEnumerable<string> DamageSources { get; set; }
    }
}
