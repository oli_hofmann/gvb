﻿using System.Collections.Generic;
using LotharSurvivors.Models.Constants;

namespace LotharSurvivors.Models.DTOs
{
    public class DamageCaseDto : AddressDto
    {
        public long Id { get; set; }

        public CaseStatus Status { get; set; }

        public IEnumerable<DamagedObjectCaseDto> DamagedObjectCases { get; set; }

        public string EventDescription { get; set; }
    }
}
