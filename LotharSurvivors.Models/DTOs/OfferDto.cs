﻿using LotharSurvivors.Models.Constants;

namespace LotharSurvivors.Models.DTOs
{
    public class OfferDto
    {
        public long Id { get; set; }

        public long CraftmanId { get; set; }

        public long ObjectId { get; set; }

        public CraftmanDamagedObjectOfferStatus Status { get; set; }
    }
}
