﻿using System;
using System.IO;
using LotharSurvivors.Models.Database;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Internal;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;

namespace LotharSurvivors.EntityFramework
{
    public class LotharDbContext : DbContext
    {
        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.Entity<Craftman>()
                .HasMany(c => c.CraftmanDamageSourcesMappings)
                .WithOne(m => m.Craftman);

            modelBuilder.Entity<DamageSource>()
                .HasMany(dS => dS.CraftmanDamageSourcesMappings)
                .WithOne(m => m.DamageSource);

            modelBuilder.Entity<DamagedObjectCase>()
                .HasMany(o => o.DamagedObjectCaseDamageSourceMappings)
                .WithOne(m => m.DamagedObjectCase);

            modelBuilder.Entity<DamageSource>()
                .HasMany(dS => dS.DamagedObjectCaseDamageSourceMappings)
                .WithOne(m => m.DamageSource);
        }

        public LotharDbContext(DbContextOptions<LotharDbContext> options) : base(options) { }

        public DbSet<Client> Clients { get; set; }

        public DbSet<Craftman> Craftmen { get; set; }
        public DbSet<CraftmanDamageSourcesMapping> CraftmanDamageSourcesMappings { get; set; }
        
        public DbSet<DamageSource> DamageSources { get; set; }

        public DbSet<DamagedObjectCaseDamageSourceMapping> DamagedObjectCaseDamageSourceMappings { get; set; }
        public DbSet<DamagedObjectCase> DamagedObjectCases { get; set; }

        public DbSet<DamageCase> DamageCases { get; set; }

        public DbSet<CraftmanDamagedObjectOffer> CraftmanDamagedObjectOffers { get; set; }
        
        public static void Bind(IServiceCollection services, IConfiguration configuration)
        {
            string dbPathAndName = configuration.GetValue<string>("dataBaseName");

            Console.WriteLine("Adding DbContext with database: " + dbPathAndName);

            CheckAccessRights(dbPathAndName);

            services.AddDbContext<LotharDbContext>(options => options.UseSqlite(
                @"Data Source=" + dbPathAndName, 
                b => b.MigrationsAssembly("LotharSurvivors.Web")));
        }

        private static void CheckAccessRights(string dbPathAndName)
        {
            var fi = new FileInfo(dbPathAndName);

            var fileExists = File.Exists(dbPathAndName);
            Console.WriteLine($"DbContext file exists: {fileExists}");

            var directoryContent = Directory.GetFiles(fi.DirectoryName).Join();
            Console.WriteLine($"Files in Db-Directory: {directoryContent}");

            try
            {
                File.WriteAllText(Path.Combine(fi.DirectoryName, "testfile"), "FileWriteTest");
                Console.WriteLine("Successfully written file to Db-Directory.");
            }
            catch (Exception ex)
            {
                Console.WriteLine($"ERROR: writing file. {ex.GetType()}: {ex.Message}");
            }

            try
            {
                var db = File.ReadAllBytes(dbPathAndName);
                Console.WriteLine($"Successfully read DB File (File-Size: {db.Length})");
            }
            catch (Exception ex)
            {
                Console.WriteLine($"ERROR: reading file. {ex.GetType()}: {ex.Message}");
            }
        }
    }
}
