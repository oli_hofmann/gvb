﻿using LotharSurvivors.EntityFramework;
using LotharSurvivors.Services;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;

namespace LotharSurvivors.Web
{
    public class Startup
    {
        public Startup(IConfiguration configuration)
        {
            Configuration = configuration;
        }

        public IConfiguration Configuration { get; }

        // This method gets called by the runtime. Use this method to add services to the container.
        public void ConfigureServices(IServiceCollection services)
        {
            services.AddMvc().SetCompatibilityVersion(CompatibilityVersion.Version_2_1);
            LotharSurvivors.EntityFramework.LotharDbContext.Bind(services, Configuration);

            services.AddTransient<OfferService>();
            services.AddTransient<ClientService>();
            services.AddTransient<CaseService>();

            // Add LanguageProcessingService and bind the configuration to the instance.
            services.AddTransient(provider =>
            {
                var lps = new LanguageProcessingService();
                Configuration.GetSection("languageProcessingServiceConfiguration").Bind(lps);
                return lps;
            });

            services.AddTransient<CraftmanService>();
        }

        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public void Configure(IApplicationBuilder app, IHostingEnvironment env)
        {
			app.UseCors(builder =>
			{
				builder.WithOrigins("*").AllowAnyHeader().WithMethods("GET", "PUT", "POST", "DELETE", "OPTIONS");
			});

			if (env.IsDevelopment())
			{
				app.UseDeveloperExceptionPage();
			}

			app.UseMvc();

            using (var serviceScope = app.ApplicationServices.GetService<IServiceScopeFactory>().CreateScope())
            {
                var context = serviceScope.ServiceProvider.GetRequiredService<LotharDbContext>();
                context.Database.Migrate();
            }
        }
    }
}
