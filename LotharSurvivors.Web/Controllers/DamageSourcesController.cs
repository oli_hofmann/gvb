﻿using System.Collections.Generic;
using System.Linq;
using LotharSurvivors.EntityFramework;
using Microsoft.AspNetCore.Mvc;

namespace LotharSurvivors.Web.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class DamageSourcesController : ControllerBase
    {
        private readonly LotharDbContext _dbContext;

        public DamageSourcesController(LotharDbContext dbContext)
        {
            _dbContext = dbContext;
        }

        [HttpGet]
        public IEnumerable<string> GetList()
        {
            return _dbContext.DamageSources.OrderBy(ds => ds.Name).Select(ds => ds.Name);
        }
    }
}