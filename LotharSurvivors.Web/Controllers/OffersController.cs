﻿using System.Collections.Generic;
using LotharSurvivors.Models.DTOs;
using LotharSurvivors.Services;
using Microsoft.AspNetCore.Mvc;

namespace LotharSurvivors.Web.Controllers
{
    [ApiController]
    public class OffersController : ControllerBase
    {
        private readonly OfferService _offerService;

        public OffersController(OfferService offerService)
        {
            _offerService = offerService;
        }

        [Route("api/cases/{damageCaseId}/objects/{objectId}/offers")]
        [HttpGet]
        public IEnumerable<OfferDto> Get(long damageCaseId, long damageObjectCaseId)
        {
            return _offerService.GetAllOffersForObjectCase(damageObjectCaseId);
        }

        [HttpPut]
        [Route("api/cases/{damageCaseId}/objects/{objectId}/offers/{offerId}")]
        public OfferDto Put(OfferDto offerDto)
        {
            return _offerService.UpdateOfferStatus(offerDto.Id, offerDto.Status);
        }

        [Route("api/cases/{damageCaseId}/objects/{objectId}/offers/{offerId}/complete")]
        [HttpPut]
        public void Complete(long offerId, OfferCompletionDto offerCompletionDto)
        {
            _offerService.CompleteOffer(offerId, offerCompletionDto);
        }
    }
}