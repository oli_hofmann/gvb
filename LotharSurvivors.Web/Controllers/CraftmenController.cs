﻿using System.Collections.Generic;
using LotharSurvivors.Models.DTOs;
using LotharSurvivors.Services;
using Microsoft.AspNetCore.Mvc;

namespace LotharSurvivors.Web.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class CraftmenController : ControllerBase
    {
        private readonly OfferService _offerService;
        private readonly CraftmanService _craftmanService;

        public CraftmenController(OfferService offerService, CraftmanService craftmanService)
        {
            _offerService = offerService;
            _craftmanService = craftmanService;
        }

        [HttpGet("{craftmanId}/offers")]
        public IEnumerable<OfferDto> GetOffersByCraftmanId(long craftmanId)
        {
            return _offerService.GetAllOffersForCraftman(craftmanId);
        }

        [HttpPost("{craftmanId}/requestOffer")]
        public OfferDto RequestOffer(long craftmanId, [FromBody] OfferRequestDto offerRequestDto)
        {
            return _offerService.RequestOffer(offerRequestDto.DamagedObjectCaseId, craftmanId);
        }

        [HttpPost]
        public CraftmanDto Post([FromBody] CraftmanDto craftmanDto)
        {
            return _craftmanService.Create(craftmanDto);
        }

        [HttpPut]
        public CraftmanDto Put([FromBody] CraftmanDto craftmanDto)
        {
            return _craftmanService.Update(craftmanDto);
        }
    }
}