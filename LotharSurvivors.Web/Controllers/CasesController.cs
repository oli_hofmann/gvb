﻿using System.Collections.Generic;
using LotharSurvivors.Models.DTOs;
using LotharSurvivors.Services;
using Microsoft.AspNetCore.Mvc;

namespace LotharSurvivors.Web.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class CasesController
    {
        private readonly CaseService _caseService;

        public CasesController(CaseService caseService)
        {
            _caseService = caseService;
        }

        [HttpPost]
        public long PostDamageReport([FromBody] DamageReportDto damageReportDto)
        {
            return _caseService.CreateCase(damageReportDto).Id;
        }

        [HttpGet("{damageCaseId}")]
        public DamageCaseDto Get(long damageCaseId)
        {
            return _caseService.Get(damageCaseId);
        }

        [HttpGet()]
        public IEnumerable<DamageCaseDto> Get()
        {
            return _caseService.GetAll();
        }
    }
}
