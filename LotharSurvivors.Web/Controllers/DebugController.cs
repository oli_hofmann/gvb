﻿using System;
using System.Collections.Generic;
using System.Linq;
using Microsoft.AspNetCore.Mvc;

namespace LotharSurvivors.Web.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class DebugController : ControllerBase
    {
        public Dictionary<string, string> Get()
        {
            return new Dictionary<string, string>
            {
                {"Greetings", "Hello"},
                {"Name", "You"},
                {"TimeLeft", (new DateTime(2018, 08, 26, 10, 00, 00) - DateTime.Now).ToString()},
                {"ReasonForThisOutput", "Absolutely none"}
            };
        }
    }
}