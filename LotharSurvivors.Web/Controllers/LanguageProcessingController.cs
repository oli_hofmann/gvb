﻿using System.Collections.Generic;
using Microsoft.AspNetCore.Mvc;
using LotharSurvivors.Models.DTOs;
using LotharSurvivors.Services;

namespace LotharSurvivors.Web.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class LanguageProcessingController : ControllerBase
    {
        private readonly LanguageProcessingService _languageProcessingService;

        public LanguageProcessingController(LanguageProcessingService languageProcessingService)
        {
            _languageProcessingService = languageProcessingService;
        }

        [HttpPost("getObjectsFromDescription")]
        public IEnumerable<string> GetObjectsFromDescription([FromBody] LanguageProcessingRequestDto data)
        {
            return _languageProcessingService.GetObjectListFromText(data.Description);
        } 
    }
}