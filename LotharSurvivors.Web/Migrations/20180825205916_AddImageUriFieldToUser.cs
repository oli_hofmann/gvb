﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace LotharSurvivors.Web.Migrations
{
    public partial class AddImageUriFieldToUser : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<string>(
                name: "ImageUri",
                table: "Craftmen",
                nullable: true);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "ImageUri",
                table: "Craftmen");
        }
    }
}
