﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

namespace LotharSurvivors.Web.Migrations
{
    public partial class Init : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.CreateTable(
                name: "Clients",
                columns: table => new
                {
                    Id = table.Column<long>(nullable: false)
                        .Annotation("Sqlite:Autoincrement", true),
                    FirstName = table.Column<string>(nullable: true),
                    LastName = table.Column<string>(nullable: true),
                    Street = table.Column<string>(nullable: true),
                    ZipCode = table.Column<string>(nullable: true),
                    City = table.Column<string>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Clients", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "Craftmen",
                columns: table => new
                {
                    Id = table.Column<long>(nullable: false)
                        .Annotation("Sqlite:Autoincrement", true),
                    FirstName = table.Column<string>(nullable: true),
                    LastName = table.Column<string>(nullable: true),
                    CompanyName = table.Column<string>(nullable: true),
                    Street = table.Column<string>(nullable: true),
                    ZipCode = table.Column<string>(nullable: true),
                    City = table.Column<string>(nullable: true),
                    EMail = table.Column<string>(nullable: true),
                    PhoneNumber = table.Column<string>(nullable: true),
                    Description = table.Column<string>(nullable: true),
                    Activated = table.Column<bool>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Craftmen", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "DamageSources",
                columns: table => new
                {
                    Id = table.Column<long>(nullable: false)
                        .Annotation("Sqlite:Autoincrement", true),
                    Name = table.Column<string>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_DamageSources", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "DamageCases",
                columns: table => new
                {
                    Id = table.Column<long>(nullable: false)
                        .Annotation("Sqlite:Autoincrement", true),
                    DateOfEvent = table.Column<DateTime>(nullable: false),
                    EventDescription = table.Column<string>(nullable: true),
                    ClientId = table.Column<long>(nullable: true),
                    Token = table.Column<string>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_DamageCases", x => x.Id);
                    table.ForeignKey(
                        name: "FK_DamageCases_Clients_ClientId",
                        column: x => x.ClientId,
                        principalTable: "Clients",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateTable(
                name: "CraftmanDamageSourcesMappings",
                columns: table => new
                {
                    Id = table.Column<long>(nullable: false)
                        .Annotation("Sqlite:Autoincrement", true),
                    CraftmanId = table.Column<long>(nullable: true),
                    DamageSourceId = table.Column<long>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_CraftmanDamageSourcesMappings", x => x.Id);
                    table.ForeignKey(
                        name: "FK_CraftmanDamageSourcesMappings_Craftmen_CraftmanId",
                        column: x => x.CraftmanId,
                        principalTable: "Craftmen",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_CraftmanDamageSourcesMappings_DamageSources_DamageSourceId",
                        column: x => x.DamageSourceId,
                        principalTable: "DamageSources",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateTable(
                name: "DamagedObjectCases",
                columns: table => new
                {
                    Id = table.Column<long>(nullable: false)
                        .Annotation("Sqlite:Autoincrement", true),
                    ClientId = table.Column<long>(nullable: true),
                    Name = table.Column<string>(nullable: true),
                    DamageCaseId = table.Column<long>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_DamagedObjectCases", x => x.Id);
                    table.ForeignKey(
                        name: "FK_DamagedObjectCases_Clients_ClientId",
                        column: x => x.ClientId,
                        principalTable: "Clients",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_DamagedObjectCases_DamageCases_DamageCaseId",
                        column: x => x.DamageCaseId,
                        principalTable: "DamageCases",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateTable(
                name: "CraftmanDamagedObjectOffers",
                columns: table => new
                {
                    Id = table.Column<long>(nullable: false)
                        .Annotation("Sqlite:Autoincrement", true),
                    DamagedObjectCaseId = table.Column<long>(nullable: true),
                    CraftmanId = table.Column<long>(nullable: true),
                    Status = table.Column<int>(nullable: false),
                    Rating = table.Column<double>(nullable: false),
                    Review = table.Column<string>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_CraftmanDamagedObjectOffers", x => x.Id);
                    table.ForeignKey(
                        name: "FK_CraftmanDamagedObjectOffers_Craftmen_CraftmanId",
                        column: x => x.CraftmanId,
                        principalTable: "Craftmen",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_CraftmanDamagedObjectOffers_DamagedObjectCases_DamagedObjectCaseId",
                        column: x => x.DamagedObjectCaseId,
                        principalTable: "DamagedObjectCases",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateTable(
                name: "DamagedObjectCaseDamageSourceMappings",
                columns: table => new
                {
                    Id = table.Column<long>(nullable: false)
                        .Annotation("Sqlite:Autoincrement", true),
                    DamagedObjectCaseId = table.Column<long>(nullable: false),
                    DamageSourceId = table.Column<long>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_DamagedObjectCaseDamageSourceMappings", x => x.Id);
                    table.ForeignKey(
                        name: "FK_DamagedObjectCaseDamageSourceMappings_DamageSources_DamageSourceId",
                        column: x => x.DamageSourceId,
                        principalTable: "DamageSources",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_DamagedObjectCaseDamageSourceMappings_DamagedObjectCases_DamagedObjectCaseId",
                        column: x => x.DamagedObjectCaseId,
                        principalTable: "DamagedObjectCases",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateIndex(
                name: "IX_CraftmanDamagedObjectOffers_CraftmanId",
                table: "CraftmanDamagedObjectOffers",
                column: "CraftmanId");

            migrationBuilder.CreateIndex(
                name: "IX_CraftmanDamagedObjectOffers_DamagedObjectCaseId",
                table: "CraftmanDamagedObjectOffers",
                column: "DamagedObjectCaseId");

            migrationBuilder.CreateIndex(
                name: "IX_CraftmanDamageSourcesMappings_CraftmanId",
                table: "CraftmanDamageSourcesMappings",
                column: "CraftmanId");

            migrationBuilder.CreateIndex(
                name: "IX_CraftmanDamageSourcesMappings_DamageSourceId",
                table: "CraftmanDamageSourcesMappings",
                column: "DamageSourceId");

            migrationBuilder.CreateIndex(
                name: "IX_DamageCases_ClientId",
                table: "DamageCases",
                column: "ClientId");

            migrationBuilder.CreateIndex(
                name: "IX_DamagedObjectCaseDamageSourceMappings_DamageSourceId",
                table: "DamagedObjectCaseDamageSourceMappings",
                column: "DamageSourceId");

            migrationBuilder.CreateIndex(
                name: "IX_DamagedObjectCaseDamageSourceMappings_DamagedObjectCaseId",
                table: "DamagedObjectCaseDamageSourceMappings",
                column: "DamagedObjectCaseId");

            migrationBuilder.CreateIndex(
                name: "IX_DamagedObjectCases_ClientId",
                table: "DamagedObjectCases",
                column: "ClientId");

            migrationBuilder.CreateIndex(
                name: "IX_DamagedObjectCases_DamageCaseId",
                table: "DamagedObjectCases",
                column: "DamageCaseId");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "CraftmanDamagedObjectOffers");

            migrationBuilder.DropTable(
                name: "CraftmanDamageSourcesMappings");

            migrationBuilder.DropTable(
                name: "DamagedObjectCaseDamageSourceMappings");

            migrationBuilder.DropTable(
                name: "Craftmen");

            migrationBuilder.DropTable(
                name: "DamageSources");

            migrationBuilder.DropTable(
                name: "DamagedObjectCases");

            migrationBuilder.DropTable(
                name: "DamageCases");

            migrationBuilder.DropTable(
                name: "Clients");
        }
    }
}
