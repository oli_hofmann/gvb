import Vue from 'vue';
import Router from 'vue-router';
import Index from '@/components/Pages/Index';
import CraftsmenHome from '@/components/Pages/CraftsmenHome';
import CreateCase from '@/components/Pages/CreateCase';
import CaseOverview from '@/components/Pages/CaseOverview';

Vue.use(Router);

export default new Router({
  routes: [
    {
      path: '/',
      name: 'home',
      component: Index
    },{
      path: '/handwerker',
      name: 'craftsmenhome',
      component: CraftsmenHome
    },{
      path: '/schadenmeldung',
      name: 'createcase',
      component: CreateCase
    },{
      path: '/schadensuebersicht/:caseId',
      name: 'caseoverview',
      component: CaseOverview,
      props: true
    }
  ]
});
