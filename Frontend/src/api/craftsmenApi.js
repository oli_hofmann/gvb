import apiConfig from './apiConfiguration';
import axios from 'axios';

export default {
    getAllOffers(craftsmenId) {
        return axios.create({
            timeout: 5000,
            headers: { 'Content-Type': 'application/json' }
        }).get(apiConfig.craftmenApi + '/' + craftsmenId + '/offers');
    }
};