
const craftmenUrl = '/api/craftmen';
const casesUrl = '/api/cases';
const languageProcessingUrl = '/api/languageProcessing';

export default {
    craftmenApi: craftmenUrl,
    casesApi: casesUrl,
    languageProcessingApi: languageProcessingUrl,
};