import apiConfig from './apiConfiguration';
import axios from 'axios';

export default {
    getObjectsFromDescription(description) {
        return axios.create({
            timeout: 5000,
            headers: { 'Content-Type': 'application/json' }
        }).post(apiConfig.languageProcessingApi + '/getObjectsFromDescription', { description: description });
    }
};