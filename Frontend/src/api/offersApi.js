import apiConfig from './apiConfiguration';
import axios from 'axios';

export default {
    requestOffer(damagedObjectId, craftmanId) {
        var url = apiConfig.craftmenApi + '/' + craftmanId + '/requestoffer';
        
        return axios.create({
            timeout: 5000,
            headers: { 'Content-Type': 'application/json' }
        }).post(url, { 'DamagedObjectCaseId': damagedObjectId });
    },
    setOfferStatus(caseId, objectId, offerId, craftmanId, newStatus) {
        var url = apiConfig.casesApi + '/' + caseId + '/objects/' + objectId + '/offers/' + offerId;

        return axios.create({
            timeout: 5000,
            headers: { 'Content-Type': 'application/json' }
        }).put(url, { 'id': offerId, 'craftmanId': craftmanId, 'objectId': objectId, 'status': newStatus });
    },
    rateAndClose(caseId, objectId, offerId, rating) {
        var url = apiConfig.casesApi + '/' + caseId + '/objects/' + objectId + '/offers/' + offerId + '/complete';

        return axios.create({
            timeout: 5000,
            headers: { 'Content-Type': 'application/json' }
        }).put(url, { 'rating': rating });
    }
};