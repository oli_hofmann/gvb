import apiConfig from './apiConfiguration';
import axios from 'axios';

export default {
    createCase(caseData) {
        // Aufbereiten der Daten
        caseData.damagedObjects = caseData.damagedObjects.filter(function (item) { return item.length > 0; });
        
        return axios.create({
            timeout: 5000,
            headers: { 'Content-Type': 'application/json' }
        }).post(apiConfig.casesApi, caseData);
    },
    getCase(caseId) {
        return axios.create({
            timeout: 5000,
            headers: { 'Content-Type': 'application/json' }
        }).get(apiConfig.casesApi + '/' + caseId);
    },
    getAll() {
        return axios.create({
            timeout: 5000,
            headers: { 'Content-Type': 'application/json' }
        }).get(apiConfig.casesApi);
    }
};