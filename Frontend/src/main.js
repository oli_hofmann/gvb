// The Vue build version to load with the `import` command
// (runtime-only or standalone) has been set in webpack.base.conf with an alias.
import Vue from 'vue'
import BootstrapVue from "bootstrap-vue"
import App from './App'
import router from './router'

/* Components */
import AppHeader from './components/Layout/AppHeader';
import Loader from './components/Tools/Loader';

/* shared store */
import Store from './sharedStore';

/* Base styling */
import 'bootstrap/dist/css/bootstrap.min.css'
import 'bootstrap-vue/dist/bootstrap-vue.css'

Vue.use(BootstrapVue)
Vue.config.productionTip = false

Vue.component('AppHeader', AppHeader);
Vue.component('Loader', Loader);

/* eslint-disable no-new */
new Vue({
  el: '#app',
  data: { store: Store },
  router,
  template: '<App/>',
  components: { App }
})
