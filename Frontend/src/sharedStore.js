
export default {
    state: {
        userLoggedIn: false
    },
    loginUser() {
        this.userLoggedIn = true;
    },
    logoffUser() {
        this.userLoggedIn = false;
    }
}