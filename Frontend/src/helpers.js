import Moment from 'moment';

export default {
    formatDate: function(date) {
        return Moment(date).format('DD.MM.YYYY');
    }
}