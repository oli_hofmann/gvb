# Build REST-API (.NET core)
Write-Host "Building .NET core backend containers..."
docker build -t lotharsurvivorsapi -f .\LotharSurvivors.Web\Dockerfile .

# Build NLP Services (Python)
Write-Host "Building Python containers for NLP..."
docker build -t lotharsurvivorscategorization .\LotharSurvivors.Categorization\
docker build -t lotharsurvivorsentityextraction .\LotharSurvivors.EntityExtraction\

# Build Frontend
Write-Host "Building nodejs frontend container..."
docker build -t lotharsurvivorsfrontend .\Frontend\

Write-Host "Done, logging in to docker hub..."
docker login

Write-Host "Done, tagging everything..."
docker tag lotharsurvivorsapi cke0218/lotharsurvivorsapi:auto
docker tag lotharsurvivorscategorization cke0218/lotharsurvivorscategorization:auto
docker tag lotharsurvivorsentityextraction cke0218/lotharsurvivorsentityextraction:auto
docker tag lotharsurvivorsfrontend cke0218/lotharsurvivorsfrontend:auto

Write-Host "Done, now pushing to docker registry..."
docker push cke0218/lotharsurvivorsapi:auto
docker push cke0218/lotharsurvivorscategorization:auto
docker push cke0218/lotharsurvivorsentityextraction:auto
docker push cke0218/lotharsurvivorsfrontend:auto

Write-Host "Done."