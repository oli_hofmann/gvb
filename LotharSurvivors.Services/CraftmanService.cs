﻿using System.Collections.Generic;
using System.Linq;
using LotharSurvivors.EntityFramework;
using LotharSurvivors.Models.Constants;
using LotharSurvivors.Models.Database;
using LotharSurvivors.Models.DTOs;
using Microsoft.EntityFrameworkCore;

namespace LotharSurvivors.Services
{
    public class CraftmanService
    {
        private readonly LotharDbContext _dbContext;

        public CraftmanService(LotharDbContext dbContext)
        {
            _dbContext = dbContext;
        }

        public CraftmanDto Create(CraftmanDto craftmanDto)
        {
            var craftman = new Craftman
            {
                FirstName = craftmanDto.FirstName,
                LastName = craftmanDto.LastName,
                Street = craftmanDto.Street,
                ZipCode = craftmanDto.ZipCode,
                City = craftmanDto.City,
                CompanyName = craftmanDto.CompanyName,
                Description = craftmanDto.Description,
                Activated = true,
                EMail = craftmanDto.Email,
                PhoneNumber = craftmanDto.PhoneNumber,
                CraftmanDamageSourcesMappings = _dbContext.Set<DamageSource>()
                    .Where(dS => craftmanDto.DamageSources.Contains(dS.Name))
                    .Select(dS => new CraftmanDamageSourcesMapping { DamageSource = dS})
            };

            _dbContext.Set<Craftman>().Add(craftman);
            _dbContext.SaveChanges();

            return ToDto(craftman);
        }

        public CraftmanDto Update(CraftmanDto craftmanDto)
        {
            var craftman = _dbContext.Set<Craftman>().First(c => c.Id == craftmanDto.Id);

            craftman.FirstName = craftmanDto.FirstName;
            craftman.LastName = craftmanDto.LastName;
            craftman.Street = craftmanDto.Street;
            craftman.ZipCode = craftmanDto.ZipCode;
            craftman.City = craftmanDto.City;
            craftman.CompanyName = craftmanDto.CompanyName;
            craftman.Description = craftmanDto.Description;
            craftman.Activated = true;
            craftman.EMail = craftmanDto.Email;
            craftman.PhoneNumber = craftmanDto.PhoneNumber;
            craftman.CraftmanDamageSourcesMappings = _dbContext.Set<DamageSource>().Where(dS => craftmanDto.DamageSources.Contains(dS.Name))
                .Select(dS => new CraftmanDamageSourcesMapping{ DamageSource = dS });

            _dbContext.Update(craftman);
            _dbContext.SaveChanges();

            return ToDto(craftman);
        }

        public IEnumerable<CraftmanDto> GetCraftmenForDamageSource(string damageSource)
        {
            return _dbContext.Set<Craftman>()
                .Where(c => c.CraftmanDamageSourcesMappings.Any(dSm => dSm.DamageSource.Name == damageSource))
                .Select(ToDto);
        }

        public double GetStarRatingForCraftman(long craftmanId)
        {
            var ratings = _dbContext.Set<Craftman>()
                .Where(c => c.Id == craftmanId)
                .SelectMany(c => c.Offers)
                .Where(o => o.Status == CraftmanDamagedObjectOfferStatus.RepairCompleted)
                .Select(o => o.Rating)
                .ToList();

            return ratings.Any() ? ratings.Average() : -1.0;
        }    

        public CraftmanDto ToDto(Craftman craftman)
        {
            return new CraftmanDto
            {
                Id = craftman.Id,
                FirstName = craftman.FirstName,
                LastName = craftman.LastName,
                Street = craftman.Street,
                ZipCode = craftman.ZipCode,
                City = craftman.City,
                StarRating = GetStarRatingForCraftman(craftman.Id),
                Description = craftman.Description,
                PhoneNumber = craftman.PhoneNumber,
                Email = craftman.EMail,
                CompanyName = craftman.CompanyName,
                ImageUrl = craftman.ImageUri
            };
        }
    }
}
