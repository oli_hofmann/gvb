﻿using System.Collections.Generic;
using System.Linq;
using LotharSurvivors.EntityFramework;
using LotharSurvivors.Models.Constants;
using LotharSurvivors.Models.Database;
using LotharSurvivors.Models.DTOs;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Query.Internal;

namespace LotharSurvivors.Services
{
    public class OfferService
    {
        private readonly LotharDbContext _dbContext;

        public OfferService(LotharDbContext dbContext)
        {
            _dbContext = dbContext;
        }

        public IEnumerable<OfferDto> GetAllOffersForObjectCase(long damagedObjectCaseId)
        {
            return _dbContext.Set<CraftmanDamagedObjectOffer>()
                .Where(o => o.DamagedObjectCase.Id == damagedObjectCaseId)
                .AsEnumerable()
                .Select(ToDto);
        }

        public IEnumerable<OfferDto> GetAllOffersForCraftman(long craftmanId)
        {
            return _dbContext.Set<CraftmanDamagedObjectOffer>()
                .Where(c => c.CraftmanId == craftmanId)
                .OrderBy(c => c.Status)
                .Select(ToDto);
        }

        public OfferDto RequestOffer(long damagedObjectCaseId, long craftmanId)
        {
            var offer = new CraftmanDamagedObjectOffer
            {
                Craftman = _dbContext.Set<Craftman>().First(c => c.Id == craftmanId),
                DamagedObjectCase = _dbContext.Set<DamagedObjectCase>().Single(c => c.Id == damagedObjectCaseId),
                Status = CraftmanDamagedObjectOfferStatus.ClientRequestsOffer
            };

            _dbContext.Set<CraftmanDamagedObjectOffer>().Add(offer);
            _dbContext.SaveChanges();

            return ToDto(offer);
        }

        public OfferDto UpdateOfferStatus(long craftmanDamagedObjectOfferId, CraftmanDamagedObjectOfferStatus status)
        {
            var offer = GetOfferById(craftmanDamagedObjectOfferId);
            offer.Status = status;

            _dbContext.SaveChanges();

            return ToDto(offer);
        }

        public OfferDto CompleteOffer(long craftmanDamagedObjectOfferId, OfferCompletionDto offerCompletionDto)
        {
            var offer = GetOfferById(craftmanDamagedObjectOfferId);

            offer.Status = CraftmanDamagedObjectOfferStatus.RepairCompleted;
            offer.Rating = offerCompletionDto.Rating;
            offer.Review = offerCompletionDto.Review;

            _dbContext.SaveChanges();
            return ToDto(offer);
        }

        private CraftmanDamagedObjectOffer GetOfferById(long craftmanDamagedObjectOfferId)
        {
            return _dbContext.Set<CraftmanDamagedObjectOffer>().Single(o => o.Id == craftmanDamagedObjectOfferId);
        }

        public OfferDto ToDto(CraftmanDamagedObjectOffer offer)
        {
            return new OfferDto
            {
                Id = offer.Id,
                CraftmanId = offer.CraftmanId,
                ObjectId = offer.DamagedObjectCaseId,
                Status = offer.Status
            };
        }
    }
}
