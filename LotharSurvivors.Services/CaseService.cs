﻿using System;
using System.Collections.Generic;
using System.Linq;
using LotharSurvivors.EntityFramework;
using LotharSurvivors.Models.Constants;
using LotharSurvivors.Models.Database;
using LotharSurvivors.Models.DTOs;
using Microsoft.EntityFrameworkCore;

namespace LotharSurvivors.Services
{
    public class CaseService
    {
        private readonly LotharDbContext _dbContext;
        private readonly ClientService _clientService;
        private readonly CraftmanService _craftmanService;
        private readonly OfferService _offerService;
        private readonly LanguageProcessingService _languageProcessingService;

        public CaseService(
            LotharDbContext dbContext, 
            ClientService clientService, 
            CraftmanService craftmanService,
            OfferService offerService,
            LanguageProcessingService languageProcessingService)
        {
            _dbContext = dbContext;
            _clientService = clientService;
            _craftmanService = craftmanService;
            _offerService = offerService;
            _languageProcessingService = languageProcessingService;
        }

        public static string RandomString(int length)
        {
            const string chars = "ABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789";
            var seed = new Random();

            return new string(Enumerable.Repeat(chars, length).Select(s => s[seed.Next(s.Length)]).ToArray());
        }

        private DamageSource GetDamageSourceForText(string text)
        {
            var damageSourceName = _languageProcessingService.GetDamageSourcesFromText(text).First().Trim();
            var damageSource = _dbContext.Set<DamageSource>().FirstOrDefault(dS => dS.Name == damageSourceName);

            if (damageSource != null)
            {
                return damageSource;
            }

            damageSource = new DamageSource {Name = damageSourceName};
            _dbContext.Set<DamageSource>().Add(damageSource);
            _dbContext.SaveChanges();

            return damageSource;
        }

        public DamageCaseDto CreateCase(DamageReportDto damageReportDto)
        {
            var client = _clientService.GetClient(damageReportDto);
            var damageSource = GetDamageSourceForText(damageReportDto.EventDescription);

            var objectList = new List<DamagedObjectCase>();
            foreach (var objectCase in damageReportDto.DamagedObjects)
            {
                var damagedObjectCase = new DamagedObjectCase
                {
                    Name = objectCase,
                    DamagedObjectCaseDamageSourceMappings = new [] { new DamagedObjectCaseDamageSourceMapping { DamageSource = damageSource }  }
                };
                _dbContext.Set<DamagedObjectCase>().Add(damagedObjectCase);
                objectList.Add(damagedObjectCase);
            }
            
            var damageCase = new DamageCase
            {
                Client =  client,
                EventDescription = damageReportDto.EventDescription,
                DateOfEvent = damageReportDto.DateOfEvent,
                Token = RandomString(24),
                DamagedObjectCases = objectList
            };

            _dbContext.Set<DamageCase>().Add(damageCase);
            _dbContext.SaveChanges();

            return ToDto(damageCase);
        }

        public DamageCaseDto Get(long damageCaseId)
        {
            return _dbContext.Set<DamageCase>()
                .Include(c => c.DamagedObjectCases)
                .Include(c => c.Client)
                .Where(c => c.Id == damageCaseId)
                .Select(ToDto)
                .Single();
        }

        public IEnumerable<DamageCaseDto> GetAll()
        {
            return _dbContext.Set<DamageCase>()
                .Include(c => c.DamagedObjectCases)
                .Include(c => c.Client)
                .Select(ToDto);
        }

        public bool IsCaseClosed(long caseId)
        {
            return !_dbContext
                .Set<DamageCase>()
                .Where(c => c.Id == caseId)
                .SelectMany(c => c.DamagedObjectCases)
                .Any(c => Enumerable.All(c.Offers, o => o.Status != CraftmanDamagedObjectOfferStatus.RepairCompleted));
        }

        private DamageCaseDto ToDto(DamageCase damageCase)
        {
            return new DamageCaseDto
            {
                FirstName = damageCase.Client.FirstName,
                LastName = damageCase.Client.LastName,
                Street = damageCase.Client.Street,
                City = damageCase.Client.City,
                ZipCode = damageCase.Client.ZipCode,
                Id = damageCase.Id,
                EventDescription = damageCase.EventDescription,
                Status = IsCaseClosed(damageCase.Id) ? CaseStatus.Closed : CaseStatus.InProgress,
                DamagedObjectCases = damageCase.DamagedObjectCases.Select(ToDto)
            };
        }

        private DamagedObjectCaseDto ToDto(DamagedObjectCase damagedObjectCase)
        {
            var dOC = _dbContext
                .Set<DamagedObjectCase>()
                .Include(c => c.DamagedObjectCaseDamageSourceMappings)
                .Include(c => c.Offers)
                .Single(c => c.Id == damagedObjectCase.Id);

            var dSources = _dbContext.Set<DamageSource>().Where(dS =>
                dOC.DamagedObjectCaseDamageSourceMappings.Any(x => x.DamageSourceId == dS.Id))
                .ToList();

            return new DamagedObjectCaseDto
            {
                Id = dOC.Id,
                Name = dOC.Name,
                DamageSources = dSources.Select(d => d.Name),
                Craftsmen = dSources.SelectMany(dS => _craftmanService.GetCraftmenForDamageSource(dS.Name)).Distinct(),
                Offers = dOC.Offers.Select(_offerService.ToDto)
            };
        }
    }
}
