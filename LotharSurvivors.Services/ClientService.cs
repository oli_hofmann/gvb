﻿using System.Linq;
using LotharSurvivors.EntityFramework;
using LotharSurvivors.Models.Database;
using LotharSurvivors.Models.DTOs;

namespace LotharSurvivors.Services
{
    public class ClientService
    {
        private readonly LotharDbContext _dbContext;

        public ClientService(LotharDbContext dbContext)
        {
            _dbContext = dbContext;
        }

        public Client GetClient(AddressDto addressDto)
        {
            var client = _dbContext.Set<Client>().SingleOrDefault(
                c => c.FirstName == addressDto.FirstName
                     && c.LastName == addressDto.LastName
                     && c.Street == addressDto.Street
                     && c.ZipCode == addressDto.ZipCode
                     && c.City == addressDto.City);

            if (client != null)
            {
                return client;
            }

            client = new Client
            {
                FirstName = addressDto.FirstName,
                LastName = addressDto.LastName,
                Street = addressDto.Street,
                ZipCode = addressDto.ZipCode,
                City = addressDto.City
            };

            _dbContext.Set<Client>().Add(client);
            _dbContext.SaveChanges();

            return client;
        }
    }
}
