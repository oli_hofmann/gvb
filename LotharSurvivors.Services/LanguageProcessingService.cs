﻿using System;
using System.Collections.Generic;
using System.Net.Http;
using System.Text;
using Newtonsoft.Json;
using Newtonsoft.Json.Serialization;

namespace LotharSurvivors.Services
{
    public class LanguageProcessingService
    {
        class ProcessingRequest
        {
            public string Text { get; set; }
        }

        class ProcessingResponse
        {
            public IEnumerable<string> Results { get; set; }
        }

        public string ObjectServiceUrl { get; set; }

        public string DamageSourceServiceUrl { get; set; }

        public IEnumerable<string> GetObjectListFromText(string text)
        {
            Console.WriteLine("Calling ObjectService at URL " + ObjectServiceUrl);
            var uri = new Uri(ObjectServiceUrl);
            return Call(uri, new ProcessingRequest {Text = text}).Results;
        }

        public IEnumerable<string> GetDamageSourcesFromText(string text)
        {
            Console.WriteLine("Calling DamageSourcesService at URL " + DamageSourceServiceUrl);
            var uri = new Uri(DamageSourceServiceUrl);
            return Call(uri, new ProcessingRequest { Text = text }).Results;
        }

        private ProcessingResponse Call(Uri uri, ProcessingRequest request)
        {
            using (var client = new HttpClient())
            {
                var json = JsonConvert.SerializeObject(request, new JsonSerializerSettings { ContractResolver = new CamelCasePropertyNamesContractResolver() });
                var response = client.PostAsync(uri, new StringContent(json, Encoding.UTF8, "application/json"));
                return JsonConvert.DeserializeObject<ProcessingResponse>(response.Result.Content.ReadAsStringAsync().Result);
            }
        }
    }
}
